package fr.zaroumia.formation.spring.dao;

import java.util.Collection;

import fr.zaroumia.formation.spring.model.Formateur;

public interface FormateurDao {

	void create(Formateur f);

	void update(Formateur f) throws Exception;

	void delete(Formateur f);

	Collection<Formateur> findAll();

	Formateur find(Long id) throws Exception;
}
