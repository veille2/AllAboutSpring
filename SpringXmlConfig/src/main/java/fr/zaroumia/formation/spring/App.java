package fr.zaroumia.formation.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.model.Formateur;
import fr.zaroumia.formation.spring.service.FormateurService;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(final String[] args) throws Exception {

		// Application Context

		ApplicationContext appContext = new ClassPathXmlApplicationContext("appContext.xml");
		FormateurService formateurService = appContext.getBean(FormateurService.class);

		// Créer un formateur
		Formateur formateur = new Formateur("Zaroual", "Mohamed", 3L, 32);
		formateurService.create(formateur);

		// Modifier un formateur
		formateur.setAge(28);
		formateurService.update(formateur);

		// Récupérer le formateur par son ID
		Formateur formateur2 = formateurService.find(2L);
		System.out.println(formateur2);

		// Supprimer le formateur
		formateurService.delete(formateur2);

		// Récupérer tous les formateurs
		System.out.println(formateurService.findAll());

	}
}
