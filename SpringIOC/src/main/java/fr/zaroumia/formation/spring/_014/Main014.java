package fr.zaroumia.formation.spring._014;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.dao.FormationDao;
import fr.zaroumia.formation.spring.service.FormationService;

public class Main014 {
public static void main (String...strings ) {
	ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("appContxt_014.xml");
	context.close();
	
}
}
