package fr.zaroumia.formation.spring._024;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({Configuration_24_Prod.class,Configuration_024_Dev.class})
public class Configuration_024 {

}
