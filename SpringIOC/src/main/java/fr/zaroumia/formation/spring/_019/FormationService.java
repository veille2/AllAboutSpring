package fr.zaroumia.formation.spring._019;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.zaroumia.formation.spring.dao.FormationDao;

@Service
public class FormationService {
	
	private FormationDao formationDao;

	@Autowired
	public FormationService(@Qualifier("formationDao2")FormationDao dao) {
		System.out.println("je sus le constructeur de formationService");
		this.formationDao = dao;

	}

	public void findAllFormation() {

		formationDao.findAll().forEach(System.out::println);

	}

}
