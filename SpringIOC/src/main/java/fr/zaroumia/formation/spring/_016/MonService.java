package fr.zaroumia.formation.spring._016;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class MonService {
	public MonService() {
		System.out.println("je suis le constructeur de MonService");
	}

}
