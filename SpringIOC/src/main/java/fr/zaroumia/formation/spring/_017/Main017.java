package fr.zaroumia.formation.spring._017;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main017 {
public static void main (String...strings ) {
	ApplicationContext context = new AnnotationConfigApplicationContext(Configuration017.class);
	FormationService formaitonService = context.getBean(FormationService.class);
	formaitonService.findAllFormation();
}
}
