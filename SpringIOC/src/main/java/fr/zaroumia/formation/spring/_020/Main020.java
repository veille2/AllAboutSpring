package fr.zaroumia.formation.spring._020;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main020 {
public static void main (String...strings ) {
	ApplicationContext context = new AnnotationConfigApplicationContext(Configuration020.class);
	DataBaseProperties2 databaseProp = context.getBean(DataBaseProperties2.class);
	System.out.println(databaseProp);
}
}
