package fr.zaroumia.formation.spring._003;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.service.FormationService;

public class Main003 {
public static void main (String...strings ) {
	ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_003.xml");
	FormationService formationService = context.getBean(FormationService.class);
	System.out.println(formationService.findAll());
}
}
