package fr.zaroumia.formation.spring._021;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.zaroumia.formation.spring.dao.FormationDao;

public class Main021 {
public static void main (String...strings ) {
	ApplicationContext context = new AnnotationConfigApplicationContext(Configuration_021.class);
	FormationDao formationDao1 = context.getBean(FormationDao.class);
	FormationDao formationDao2 = context.getBean(FormationDao.class);
	System.out.println(formationDao1 == formationDao2);
	
	}
}
