package fr.zaroumia.formation.spring._017;


import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.zaroumia.formation.spring.dao.FormationDao;

@Service
public class FormationService {
	/*@Autowired
	@Qualifier("formationDao2")*/
	@Resource(name="formationDao")
	private FormationDao dao;
	public  void findAllFormation() {
		
		dao.findAll().forEach( System.out::println);
	
	}

}
