package fr.zaroumia.formation.spring.service;

import java.util.List;

import fr.zaroumia.formation.spring.model.Formation;

public interface FormationService {
	List<Formation> findAll();
}
