package fr.zaroumia.formation.spring._005;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.dao.FormationDao;
import fr.zaroumia.formation.spring.service.FormationService;

public class Main005 {
public static void main (String...strings ) {
	ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_006.xml");
	
	Map<String, FormationDao> beans = context.getBeansOfType(FormationDao.class);
	beans.entrySet().stream().forEach(bean->System.out.println(bean.getKey()));
	
	System.out.println(context.getBean("formationDao1")==context.getBean("formationDao2"));
}
}
