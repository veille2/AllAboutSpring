package fr.zaroumia.formation.spring._007;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.service.FormationService;

public class Main007 {
public static void main (String...strings ) {
	ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_007.xml");
	Bean007 bean = context.getBean(Bean007.class);
	System.out.println(bean);
}
}
