package fr.zaroumia.formation.spring._010;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main010 {
public static void main (String...strings ) {
	
	ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_010.xml");
	DataBaseProperties dataBaseProp = context.getBean(DataBaseProperties.class);
	
	System.out.println(dataBaseProp);
}
}
