package fr.zaroumia.formation.spring._011;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.service.FormationService;


public class Main012 {
public static void main (String...strings ) {
	
	ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_011.xml");
	FormationService formationService = context.getBean(FormationService.class);
	FormationService formationService1 = context.getBean(FormationService.class);

	FormationService formationService2 = context.getBean(FormationService.class);

	System.out.println(formationService2==formationService1);
	System.out.println(formationService==formationService1);
	
}
}
