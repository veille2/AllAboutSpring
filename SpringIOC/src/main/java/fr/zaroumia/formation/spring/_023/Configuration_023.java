package fr.zaroumia.formation.spring._023;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({Configuration_023Dao.class,Configuration_023Service.class})
public class Configuration_023 {

}
