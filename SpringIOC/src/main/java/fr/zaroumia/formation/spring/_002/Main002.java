package fr.zaroumia.formation.spring._002;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Main002 {

	public static void main(String[] args) {
		//ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_002.xml");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("appContxt_002.xml");
		context.setDisplayName("myApp");
		MonBean2 monBean=		context.getBean(MonBean2.class);
		monBean.helloWorld();
	}
}
