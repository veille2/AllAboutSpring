package fr.zaroumia.formation.spring._007;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Bean007 {
	private int entier;
	private float reel;
	private String chaine;
	private boolean booleen;
	private char caractere;
	private Locale localisation;
	private Properties properties;
	private URL url;
	private File file;
	private byte[] tabeauBytes;
	private String[] tableauChaines;
	private List<String> liste;
	private Map<String, String> map;
	private Set<String> set;

	public void setEntier(int entier) {
		this.entier = entier;
	}

	public void setReel(float reel) {
		this.reel = reel;
	}

	public void setChaine(String chaine) {
		this.chaine = chaine;
	}

	public void setBooleen(boolean booleen) {
		this.booleen = booleen;
	}

	public void setCaractere(char caractere) {
		this.caractere = caractere;
	}

	public void setLocalisation(Locale localisation) {
		this.localisation = localisation;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void setTabeauBytes(byte[] tabeauBytes) {
		this.tabeauBytes = tabeauBytes;
	}

	public void setTableauChaines(String[] tableauChaines) {
		this.tableauChaines = tableauChaines;
	}

	public void setListe(List<String> liste) {
		this.liste = liste;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	public void setSet(Set<String> set) {
		this.set = set;
	}

	@Override
	public String toString() {
		return "Bean007 [entier=" + entier + ", reel=" + reel + ", chaine=" + chaine + ", booleen=" + booleen
				+ ", caractere=" + caractere + ", localisation=" + localisation + ", properties=" + properties
				+ ", url=" + url + ", file=" + file + ", tabeauBytes=" + Arrays.toString(tabeauBytes)
				+ ", tableauChaines=" + Arrays.toString(tableauChaines) + ", liste=" + liste + ", map=" + map + ", set="
				+ set + "]";
	}

}
