package fr.zaroumia.formation.spring._019;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main019 {
public static void main (String...strings ) {
	ApplicationContext context = new AnnotationConfigApplicationContext(Configuration019.class);
	FormationService formaitonService = context.getBean(FormationService.class);
	formaitonService.findAllFormation();
}
}
