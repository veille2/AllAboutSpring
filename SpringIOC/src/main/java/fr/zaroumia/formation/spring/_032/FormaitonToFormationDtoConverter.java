package fr.zaroumia.formation.spring._032;

import org.springframework.core.convert.converter.Converter;

import fr.zaroumia.formation.spring.model.Formation;

public class FormaitonToFormationDtoConverter implements Converter<Formation, FormationDto> {

	@Override
	public FormationDto convert(Formation model) {
		FormationDto dto = new FormationDto();
		dto.setId(model.getId());
		dto.setTitre(model.getTitre());
		if (model.getDescriptif() != null && model.getDescriptif().length() > 20) {
			dto.setDescriptifCourt(model.getDescriptif().substring(0,20) + "...");
		}
     return dto;
	}

}
