/**
 * 
 */
package fr.zaroumia.formation.spring._032;

/**
 * @author lydiasaighi
 *
 */
public class FormationDto {
	private Long id;
	private String titre;
	private String descriptifCourt;

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public void setDescriptifCourt(String descriptifCourt) {
		this.descriptifCourt = descriptifCourt;
	}

	@Override
	public String toString() {
		return "FormationDto [id=" + id + ", titre=" + titre + ", descriptifCourt=" + descriptifCourt + "]";
	}

}
