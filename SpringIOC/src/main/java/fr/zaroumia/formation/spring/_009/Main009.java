package fr.zaroumia.formation.spring._009;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.service.FormationService;

public class Main009 {
public static void main (String...strings ) {
	ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_008.xml");
	MySingleton bean = context.getBean(MySingleton.class);
	MySingleton bean2 = context.getBean(MySingleton.class);
	bean.quiSuisJe();
	System.out.println(bean == bean2);

	
	BeanAcreer beanACreer = context.getBean(BeanAcreer.class);
	beanACreer.quiSuisJe();
}
}
