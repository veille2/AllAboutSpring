package fr.zaroumia.formation.spring.dao;

import java.util.List;

import fr.zaroumia.formation.spring.model.Formation;

public interface FormationDao {
	String quiSuisJe();

	List<Formation> findAll();
}
