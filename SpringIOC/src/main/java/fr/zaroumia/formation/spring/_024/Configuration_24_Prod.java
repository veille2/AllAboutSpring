package fr.zaroumia.formation.spring._024;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("prod")
public class Configuration_24_Prod {

	
	@Bean
	public Logger logger() {
		return new DataBaseLogger();
	}
}
