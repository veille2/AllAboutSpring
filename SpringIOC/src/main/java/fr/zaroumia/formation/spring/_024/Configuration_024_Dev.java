package fr.zaroumia.formation.spring._024;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
public class Configuration_024_Dev {

	@Bean
	
	public Logger logger() {
		return new ConsoleLogger();
	}
	
}
