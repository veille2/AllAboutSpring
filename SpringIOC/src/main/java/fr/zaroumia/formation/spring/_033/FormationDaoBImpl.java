package fr.zaroumia.formation.spring._033;

import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import fr.zaroumia.formation.spring.dao.FormationDao;
import fr.zaroumia.formation.spring.model.Formation;

@Repository
@Primary

public class FormationDaoBImpl implements FormationDao {
	public FormationDaoBImpl() {
		System.out.println("FormationDaoBImpl : constructeur par défaut ");

	}

	@Override
	public String quiSuisJe() {
		return " je suis une implémentation B de FormationDao ";
	}

	@Override
	public List<Formation> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
