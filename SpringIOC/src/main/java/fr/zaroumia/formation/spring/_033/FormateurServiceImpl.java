package fr.zaroumia.formation.spring._033;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.zaroumia.formation.spring.dao.FormationDao;
import fr.zaroumia.formation.spring.service.FormateurService;

@Service
public class FormateurServiceImpl implements FormateurService {
@Autowired
	private FormationDao formationDaoC;

	public FormateurServiceImpl() {
		System.out.println("FormateurServiceImpl : constructeur par défaut");
		
		System.out.println( formationDaoC);
	}

	public FormateurServiceImpl(final FormationDao formationDao) {
		super();
		this.formationDaoC = formationDao;
		System.out.println("FormateurServiceImpl : constructeur avec arguements");
	}

	public void setFormationDao(final FormationDao formationDao) {
		this.formationDaoC = formationDao;
		System.out.println("FormateurServiceImpl :le setter de formationDao");
	}

}