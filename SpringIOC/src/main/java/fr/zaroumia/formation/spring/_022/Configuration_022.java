package fr.zaroumia.formation.spring._022;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import fr.zaroumia.formation.spring.dao.FormationDao;
import fr.zaroumia.formation.spring.dao.FormationDaoImpl;

@Configuration
@ComponentScan("fr.zaroumia.formation.spring._022")
public class Configuration_022 {
	
	@Bean (initMethod="init",destroyMethod="destroy")	
	public Etudiant etudiant () {
		return new Etudiant();
	}

}
