package fr.zaroumia.formation.spring._033;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.zaroumia.formation.spring.dao.FormationDao;
import fr.zaroumia.formation.spring.model.Formation;

@Repository

public class FormationDaoImpl implements FormationDao {
	public FormationDaoImpl() {
		System.out.println("FormationDaoImpl : constructeur par défaut ");

	}

	@Override
	public String quiSuisJe() {
		return " je suis une implémentation de FormationDao ";
	}

	@Override
	public List<Formation> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
