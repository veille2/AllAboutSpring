package fr.zaroumia.formation.spring._016;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.zaroumia.formation.spring.dao.FormationDao;
import fr.zaroumia.formation.spring.dao.FormationDaoImpl;

@Configuration
@ComponentScan("fr.zaroumia.formation.spring._016")
public class Configuration016 {
	
	
	@Bean
	public FormationDao createFormationDao() {
		return new FormationDaoImpl();
		
	}

}
