package fr.zaroumia.formation.spring._009;

public class MySingleton {
	private static volatile MySingleton instance;

	
	private MySingleton() {
		
	}
	public static MySingleton getInstance() {
		if(instance == null) {
			synchronized(MySingleton.class) {
				if(instance == null) {
					instance= new MySingleton();
				}
			}
		}
		return instance;
	}
	
public void quiSuisJe() {
	System.out.println("Je suis Singleton :)");
	
}
}
