package fr.zaroumia.formation.spring._002;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class MonBean2  implements ApplicationContextAware{
private ApplicationContext appCtxt;

	public void helloWorld() {
		System.out.println("Hello world from "+ appCtxt.getDisplayName());
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		this.appCtxt = arg0;
		
	}

}
