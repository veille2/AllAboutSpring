package fr.zaroumia.formation.spring._032;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.convert.ConversionService;

import fr.zaroumia.formation.spring.model.Formation;

public class Main032 {
public static void main (String...strings ) {
	ApplicationContext context = new AnnotationConfigApplicationContext(Configuration_032.class);
	Formation formation = new Formation(1L, "formtion spring", "un descriptif de plus de 20 caractères");
	ConversionService bean = context.getBean(ConversionService.class);
	System.out.println(bean.convert(formation, FormationDto.class));

	}
}
