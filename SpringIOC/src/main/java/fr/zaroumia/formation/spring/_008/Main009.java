package fr.zaroumia.formation.spring._008;

import java.text.Normalizer.Form;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.service.FormationService;

public class Main009 {
public static void main (String...strings ) {
	//ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_009-dao.xml","appContxt_009-service.xml");
	//ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_009-*.xml");
	
	ApplicationContext context = new ClassPathXmlApplicationContext("appContxt_009_all.xml");
	FormationService formaitnService = context.getBean(FormationService.class);
	
	System.out.println(formaitnService.findAll());
}
}
