package fr.zaroumia.formation.spring._015;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.zaroumia.formation.spring.dao.FormationDao;
import fr.zaroumia.formation.spring.service.FormationService;

public class Main015 {
public static void main (String...strings ) {
	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("appContxt_015.xml");
	
	
	context.getEnvironment().setActiveProfiles("dev");
	context.refresh();
	Logger consoleLogger = context.getBean(ConsoleLogger.class);
}
}
