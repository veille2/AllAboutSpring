/**
 * 
 */
package fr.zaroumia.formation.spring.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.zaroumia.formation.spring.dao.FormateurDao;
import fr.zaroumia.formation.spring.model.Formateur;

/**
 * @author lydiasaighi
 *
 */
@Service
public class FormateurServiceImpl implements FormateurService {

	@Autowired
	private FormateurDao formateurDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.zaroumia.formation.spring.service.FormateurService#create(fr.zaroumia.
	 * formation.spring.model.Formateur)
	 */
	@Override
	public void create(Formateur f) {
		formateurDao.create(f);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.zaroumia.formation.spring.service.FormateurService#update(fr.zaroumia.
	 * formation.spring.model.Formateur)
	 */
	@Override
	public void update(Formateur f) throws Exception {
		formateurDao.update(f);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.zaroumia.formation.spring.service.FormateurService#delete(fr.zaroumia.
	 * formation.spring.model.Formateur)
	 */
	@Override
	public void delete(Formateur f) {
		formateurDao.delete(f);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.zaroumia.formation.spring.service.FormateurService#findAll()
	 */
	@Override
	public Collection<Formateur> findAll() {
		return formateurDao.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.zaroumia.formation.spring.service.FormateurService#find(java.lang.Long)
	 */
	@Override
	public Formateur find(Long id) throws Exception {

		return formateurDao.find(id);
	}

	/**
	 * @param formateurDao the formateurDao to set
	 */
	public void setFormateurDao(FormateurDao formateurDao) {
		this.formateurDao = formateurDao;
	}

}
