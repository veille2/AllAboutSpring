/**
 * 
 */
package fr.zaroumia.formation.spring.dao;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Repository;

import fr.zaroumia.formation.spring.model.Formateur;

/**
 * @author lydiasaighi
 *
 */
@Repository
public class FormateurDaoImpl implements FormateurDao {

	private Set<Formateur> formateurs = new HashSet<>();

	/**
	 * 
	 */
	public FormateurDaoImpl() {
		super();
		populateFormateurs();
	}

	private void populateFormateurs() {
		Formateur formateur1 = new Formateur("nom1", "prenom1", 1L, 28);
		Formateur formateur2 = new Formateur("nom2", "prenom2", 2L, 30);

		formateurs.add(formateur1);
		formateurs.add(formateur2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.zaroumia.formation.spring.dao.FormateurDao#create(fr.zaroumia.formation.
	 * spring.model.Formateur)
	 */
	@Override
	public void create(Formateur f) {
		formateurs.add(f);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.zaroumia.formation.spring.dao.FormateurDao#update(fr.zaroumia.formation.
	 * spring.model.Formateur)
	 */
	@Override
	public void update(Formateur f) throws Exception {
		Formateur oldFormateur = find(f.getId());
		formateurs.remove(oldFormateur);
		formateurs.add(f);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.zaroumia.formation.spring.dao.FormateurDao#delete(fr.zaroumia.formation.
	 * spring.model.Formateur)
	 */
	@Override
	public void delete(Formateur f) {
		formateurs.remove(f);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.zaroumia.formation.spring.dao.FormateurDao#findAll()
	 */
	@Override
	public Collection<Formateur> findAll() {

		return formateurs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.zaroumia.formation.spring.dao.FormateurDao#find(java.lang.Long)
	 */
	@Override
	public Formateur find(Long id) throws Exception {
		return formateurs.stream().filter(formateur -> formateur.getId().equals(id)).findAny().orElseThrow(()-> new Exception("Formateur introuvable"));
	}

}
