/**
 * 
 */
package fr.zaroumia.formation.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author lydiasaighi
 *
 */
@Configuration
@ComponentScan("fr.zaroumia.formation.spring")
public class AppConfiguration {

}
